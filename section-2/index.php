<?php

require "./functions.php";

// dd($_SERVER);

// Vscode extensions: toggle semicolon

$uri = $_SERVER['REQUEST_URI'];
$dpath = "/php-for-beginners/section-2";

if ($uri === $dpath . "/") {
    require "./controllers/index.php";
} else if ($uri === $dpath . "about.php") {
    require "./controllers/about.php";
}
