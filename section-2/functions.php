<?php

function dd($value)
{
    echo "<pre>";
    var_export($value);
    echo "</pre>";

    die();
}

function urlIs($value)
{
    return $_SERVER['REQUEST_URI'] === $value;
}
