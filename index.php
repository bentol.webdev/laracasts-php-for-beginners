<?php

$books = [
    [
        'name' => 'do androids dream of electric sheep',
        'author' => 'philip k. dick',
        'releaseYear' => 1968,
        'purchaseUrl' => 'http://example.com'
    ],
    [
        'name' => 'project hail mary',
        'author' => 'andy weir',
        'releaseYear' => 2021,
        'purchaseUrl' => 'http://example.com'
    ],
    [
        'name' => 'the martian',
        'author' => 'andy weir',
        'releaseYear' => 2011,
        'purchaseUrl' => 'http://example.com'
    ]
];

$filteredBooks = array_filter($books, function ($book) {
    return $book['author'] === 'andy weir';
});

require "index.view.php";
